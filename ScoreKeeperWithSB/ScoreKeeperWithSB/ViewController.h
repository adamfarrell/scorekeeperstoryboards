//
//  ViewController.h
//  ScoreKeeperWithSB
//
//  Created by Adam Farrell on 6/29/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

//First Player
@property (strong, nonatomic) IBOutlet UITextField *Player1Name;
@property (strong, nonatomic) IBOutlet UILabel *Player1Score;
@property (strong, nonatomic) IBOutlet UIStepper *Player1Stepper;
- (IBAction)Player1Stepper:(id)sender;

//Second Player
@property (strong, nonatomic) IBOutlet UITextField *Player2Name;
@property (strong, nonatomic) IBOutlet UILabel *Player2Score;
@property (strong, nonatomic) IBOutlet UIStepper *Player2Stepper;
- (IBAction)Player2Stepper:(id)sender;

//Reset Both Players
- (IBAction)ResetScores:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ResetButton;

@end

