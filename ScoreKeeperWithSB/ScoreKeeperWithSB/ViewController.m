//
//  ViewController.m
//  ScoreKeeperWithSB
//
//  Created by Adam Farrell on 6/29/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    self.ResetButton.backgroundColor = [UIColor blueColor];
//    self.ResetButton.titleLabel.textColor = [UIColor whiteColor];
    self.Player2Name.delegate = self;
    self.Player1Name.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.Player1Name resignFirstResponder];
    [self.Player2Name resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField) {
        [textField resignFirstResponder];
    }
    return NO;
}

- (IBAction)Player1Stepper:(id)sender {
    UIStepper* stepper = (UIStepper*)sender;
    self.Player1Score.text = [NSString stringWithFormat:@"%.f", stepper.value];
}

- (IBAction)Player2Stepper:(id)sender {
    UIStepper* stepper = (UIStepper*)sender;
    self.Player2Score.text = [NSString stringWithFormat:@"%.f", stepper.value];
}

- (IBAction)ResetScores:(id)sender {
    self.Player1Score.text = @"0";
    self.Player2Score.text = @"0";
    self.Player1Stepper.value = 0;
    self.Player2Stepper.value = 0;
    self.Player1Name.text = @"";
    self.Player2Name.text = @"";
}

@end
